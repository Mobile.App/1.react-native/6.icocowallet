/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ColorPropType,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import AutoHeightImage from 'react-native-auto-height-image';
import Icon from 'react-native-vector-icons/FontAwesome';
import createGuid from "react-native-create-guid";
import QRCode from 'react-native-qrcode-svg';


const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const walletImg = require('./assets/image/walletImg.png'); 

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      walletStatus: 0,
      walletMoney: '0'
    }

    this.guidCodeWallet = '8050d2c8-7fa4-43d3-b12b-daa19091ed50';

    
  }

  getWalletMoney(){
    let str = 'https://dweet.io:443/get/latest/dweet/for/icoco_' + this.guidCodeWallet;
    return fetch(str)
      .then((response) => response.json())
      .then((json) => {        
        console.log(json);
        if(json.this == 'failed'){
          console.log('json.this failed');
          setTimeout(()=>this.getWalletMoney(), 1000);
        }
        else{
          console.log('json.this ok')
          let moneyStr = json.with[0].content.money;
          this.setState({walletMoney:moneyStr});
          setTimeout(()=>this.getWalletMoney(), 1000);
        }
      })
      .catch((error) => {
        console.error(error);
        setTimeout(()=>this.getWalletMoney(), 1000);
      });
  };

  

  componentDidMount(){
    getData = async () => {
      try {
        const value = await AsyncStorage.getItem('@idCodeWallet_Key')
        if(value !== null) {
          // value previously stored
          console.log('idCodeWallet: ');
          this.setState({walletStatus: 1});

          setTimeout(()=>this.getWalletMoney(), 1000);
        }
        else{
          console.log('idCodeWallet is null');
          this.setState({walletStatus: 0});

          setTimeout(()=>this.getWalletMoney(), 1000);
        }
      } catch(e) {
        // error reading value
        console.log("Error reading!!");
      }
    }
    getData();
  }
  
  createIdWallet(){
    console.log('create Wallet');
    createGuid().then((guid) => this.createQrcodeWallet(guid));
  }
  createQrcodeWallet(guid){
    console.log(guid);
    this.guidCodeWallet = guid;
    this.setState({walletStatus: 1});
    console.log('finish');
  }
    
  render(){
    let content;
    if(this.state.walletStatus == 0)
      content = 
        <View style={{
          flexDirection: 'column',
          flex: 1,
          zIndex: 255,
        }}>
          <View style={{
            flex: 2,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
            <Text style={{
              fontSize: (0.1*windowWidth),
              paddingTop: (0.3*windowWidth),
            }}>
              Bạn chưa tạo ví
            </Text>
          </View>
          <View style={{
            flex: 3,
            alignItems: 'center',
            zIndex: 255,            
          }}>
            <TouchableOpacity>
              <Icon
                name="cart-plus"
                color='#d8973c'
                onPress={()=>this.createIdWallet()}
                size={(0.5*windowWidth)}
              >
              </Icon>
            </TouchableOpacity>            
          </View>
        </View>
    else if(this.state.walletStatus == 1){
      content = 
        <View style={{
          flexDirection: 'column',
          flex: 1,
          backgroundColor: '#fcf5c7',
          borderRadius: (0.3*windowWidth),
          marginTop: (0.3*windowWidth),
          marginBottom: (0.3*windowWidth),
          marginRight: (0.1*windowWidth),
          marginLeft: (0.1*windowWidth),
          shadowColor: "#bfb24d",
          shadowOffset: {
            width: 0,
            height: 12,
          },
          shadowOpacity: 0.8,
          shadowRadius: 16.00,

          elevation: 24,
        }}>
          <View style={{
            flex: 2,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#f2ecbf',
            borderTopLeftRadius: (0.3*windowWidth),
            borderTopRightRadius: (0.3*windowWidth),
            
          }}>
            <Text style={{
              fontSize: (0.07*windowWidth),
              fontFamily: 'serif',
            }}>
              Số dư hiện tại
            </Text>
            <Text style={{
              fontSize: (0.1*windowWidth),
              fontFamily: 'sans-serif-medium',
            }}>
              {this.state.walletMoney}
            </Text>
          </View>
          <View style={{
            flex: 3,
            alignItems: 'center',
            paddingTop: (0.1*windowWidth),
          }}>
            <QRCode
              value={this.guidCodeWallet}
            />
          </View>
        </View>
    }
    return(
      <View 
        style={{
          backgroundColor: '#f7f7ff',
          flex: 1,
          flexDirection: 'column',
        }}
      >
        <View
          style={{
            backgroundColor: '#90e0ef',
            flex: 5,            
            borderTopLeftRadius: (0.3*windowWidth),
            borderBottomRightRadius: (0.3*windowWidth),
            justifyContent: 'center',  
            flexDirection: 'row',          
          }}
        >          
            <View style={{
              flex: 1,
              paddingTop: (0.08*windowWidth),
            }}>
              <AutoHeightImage width={(0.5*windowWidth)} source={walletImg} />
            </View>
            <View style={{
              flex: 1,
              justifyContent: 'center',              
            }}>
              <Text style={{
                fontSize: (0.14*windowWidth),
                color: '#ff6b6b',
                fontFamily: 'Hello Sunshine',
              }}>
                {"{icoco}"}
              </Text>
              <Text style={{
                fontSize: (0.1*windowWidth),
                color: '#ff6b6b',
                fontFamily: 'Hello Sunshine',
              }}>
                {"  wallet"}
              </Text>
            </View>
        </View>
          
        <View
          style={{
            flex: 14,
            
          }}
        >
          {content}
        </View>
        <View
          style={{
            backgroundColor: '#84a98c',
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Text style={{
            fontSize: (0.05*windowWidth),
          }}>
            Idea Group
          </Text>
        </View>
      </View>
    )
  }
}

export default App;
